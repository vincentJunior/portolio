import project1 from '../assets/project1.png';
import project2 from '../assets/project2.png';
import project3 from '../assets/project3.png';


const PROJECTS = [
    {
        id: 1,
        title: 'Api Project',
        description: 'A Rest API that I build from scratch with Get,Post,Put,and Delete' ,
        link: 'https://gitlab.com/vincentJunior/api',
        image: project1

    },
    {
        id: 2,
        title: 'Bkpm Project',
        description: 'Big data development, mining data from twitter.',
        link: "Can't show the code",
        image: project2

    },
    {
        id: 3,
        title: 'Internal Activity Website',
        description: 'Build website for internal member using codeigniter.',
        link: "Can't show the code",
        image: project3

    }
]


export default PROJECTS;