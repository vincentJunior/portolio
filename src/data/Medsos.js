import github from '../assets/github_icon.png';
import email from '../assets/email_icon.png';
import linkedin from '../assets/linkedin_icon.png';
import phone from '../assets/phone.png';


const Medsos = [
    {
        id: 1,
        media: 'Git Hub',
        link: 'https://gitlab.com/vincentJunior',
        image: github
    },
    {
        id: 2,
        media: 'Email',
        link: 'mailto:vincent_cars@yahoo.com',
        image: email
    },
    {
        id: 3,
        media: 'linkedin',
        link: 'https://www.linkedin.com/in/vincent-junior-123752197/',
        image: linkedin
    },
]


export default Medsos;